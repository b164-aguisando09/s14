console.log(`Hello World`);

let firstName = "Orl";
let lastName = "Aguisando";
let age = 24;
let hobbies = ['Games', 'Hiking', 'Anime'];
let address = {
	barangay: "Taculing",
	city: "Bacolod City",
	province: "Negros Occidental",
	country: "Philippines"
}


function printUserInfo(){
	console.log(`First Name: ${firstName} `);
	console.log(`Last Name: ${lastName} `);
	console.log(`Age: ${age}  `)
	console.log(`${firstName} ${lastName} is ${age} years of age.`)
	console.log(`Hobbies: ${hobbies}  `);
	console.log(`Work Address:`);
	console.log(address);
}

printUserInfo();

function returnFunction(isMarried){

	return `${isMarried}`
}		
		
let returnVar = returnFunction(false);
console.log(`The value of isMarried is: ${returnVar}`);

function add(a, b){
	let addition = a + b;
	console.log(addition)
}

add(5, 5);		


function sub(a, b){
	let subtract = a - b;
	console.log(subtract);
}

sub(10, 2);


function multiply(a, b){
	let multiplication = a * b;
	return `${multiplication}`
}

let mult = multiply(2, 3);
console.log(mult);