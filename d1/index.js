console.log("hello world");

let myVariable;
console.log (myVariable)

const constantVar = "Hello Again"
console.log ("constantVar")

let firstName = "Jane"
let	lastName = "Doe"

let product_description = "sample";

let productName = "desktop computer"
console.log(productName)

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

productName = 55234;
console.log(productName);

//global scope
let outerVariable = "hello"

{
	//block/local scope
	let innerVariable = "hello again"
	console.log(innerVariable)
}

console.log(outerVariable)

let productCode = 'DC017'
const productBrand = 'Dell'

console.log(productCode, productBrand)

/*
	Data Types
	1.String
	2.Numbers
	3.Boolean
	4.Undefined
	5.Object(array)
*/

let country = 'Philippines';
let province = "Metro Manila";
console.log(typeof country);
console.log(typeof province);

//concatenating strings
let fullAddress = province + ', ' + country;
console.log(fullAddress)

let greeting = "I live in the " + country;
console.log(greeting);

//template literals is the updated version of concantenation
//backticks ``
//expression ${}
console.log(`I live in the ${province}, ${country}`);

//escape character (\) 
//"\n" create new line in between text
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// \'

let message = 'John\'s employee went home early';
console.log(message)

message = "John's employees went home early";
console.log(message);

console.log(
	`Metro Manila


	Philippines`

	);

//Numbers
//Integers/Whole numbers

let headCount = 23;
console.log(typeof headCount);

//decimal/fraction
let grade = 98.7;
console.log(typeof grade);

//exponential notation
let planetDistance = 2e10;
console.log(typeof planetDistance);

//combining string and number
console.log("John's grade last quarter is " + grade);
console.log(`John's grade last quarter is ${grade}`);

//boolean
let isMarried = false;
let inGoodConduct = true;

console.log(typeof isMarried);
console.log(typeof inGoodConduct);
console.log(inGoodConduct);

//objects

//array

//similar data types
//syntax: let/const arrayName = [element A, element B, ...]

let grades = [98.7, 92.1, 90.2, 94.2];
console.log(grades);
console.log(typeof grades);

console.log(grades[1]);

//objects
//syntax 
/*
	let/const objectName = {propertyA: value, propertyB: value}

*/

let objectGrades = {
	firstQuarter: 98.7, 
	secondQuarter: 92.1, 
	thirdQuarter: 90.2, 
	fourthQuarter: 94.2
};

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["01241244124", '214357547'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
};

console.log(person.fullName);
console.log(`${person.fullName}, ${person.age}`);
console.log(person.contact[1]);
console.log(person.address.city);
console.log(person);

//miniactivity
let firstName1 = "Orlando";
let lastName1 = "Aguisando";
console.log(`${firstName1} ${lastName1}`);

let sentence = "is a student of Zuitt";

console.log(`${firstName1} ${lastName1} ${sentence}`);

let food = ["Fries", " Burger", " Pizza" ];
console.log(food);

let aboutMe = {

	firstName1: "Orlando",
	lastName1: "Aguisando",
	isDeveloper: true,
	hasPortfolio: true,
	age: 24
};

console.log(food);


//Numbers

let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = 3;
let num5 = 1

 //mathematical operations (+, -, *, /, %(modulo-remainder))

console.log(num1 - num2);
console.log(num1 % num2); //answer is 5; 5/6 remainder is 5. if cannot be the dividend is the modulo
console.log(num2 % num1); //answer is 1; 6/5 remainder is 1. 

//null
let girlfriend = null;

console.log(girlfriend);

let myNumber = 0;
let myString = '';

console.log(myNumber);
console.log(myString);

//undefined

let sampleVar;
console.log(sampleVar);

//function

//function decleration
	//defines a function with the function and specified parameters

/*
Syntax:

	function functionName(){
		code block/statements
	};

	function keyword - defined a js function
	function name - camelCase to name our function
	function block/statements - body of function
*/




//function expression

let variableFunction = function() {
	console.log("hello world")
};

let funcExpression = function funcName(){
	console.log("Hello")
};

//function invocation
	//the code inside a function is called

// lets call the function

printName();

//function expression call
variableFunction();

//function scoping

function myFunction() {
	let nickName = "Jane";
	console.log(nickName);
}

myFunction();

let name = "Jane Smith";

//parameters and arguments
//"name" paramenter 
//parameter - a variable inside a function
function printName(name) {
	console.log(`My name is ${name}`);
};

//arguments is the actual data in the function

printName("Jane");

printName(name);

//multiple parameters and arguments

function displayFullname(fname, lname, age){

	console.log(`${fname} ${lname} is ${age}`);
}

displayFullname("Orl", "Agusando", 25);

//return keyword

function createFullName(firstName, middleName, lastName){

	return `${firstName} ${middleName} ${lastName}`;
	console.log("I will no longer run because the function's value/result has been returned")
};


let fullName1 = createFullName("Tom", "Cruise", "Mapather")
console.log(fullName1);


let fullName2 = displayFullname("mat", "cris", 12);
console.log(fullName2);

let fullName3 = createFullName("jef", "jon", "smuit");
console.log(fullName3);

//prompt 

//MiniACtivity


let firstNumber = prompt("First Number?");
let secondNumber = prompt("Second Number?")


function division(firstNum, secondNum){
	let quotient = firstNum/secondNum;
	

	return `${quotient}`
};

let answer = division(firstNumber, secondNumber);
console.log(`The result of the division is: ${answer}`);

//function as an argument

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(){
	argumentFunction();
}

invokeFunction();

